# -*- coding: utf-8 -*-
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from static.Connection import con
from static.Contollers.API.OrdersController import OrdersController
from static.Contollers.API.SecurityController import SecurityController
from static.Contollers.API.UserController import UserController
from static.Contollers.API.RoleController import RoleController
from static.Contollers.API.ClientController import Clientcontroller
from static.Contollers.API.CategoryController import CategoryController
from static.Contollers.API.CommentController import CommentController
from static.Contollers.API.ProductController import ProductController
from static.Contollers.IndexController import IndexController
from static.Contollers.RolesController import RolesController
from static.Contollers.ClientsController import ClientsController
from static.Contollers.ProvidersController import ProvidersController
from static.Contollers.CategoriesController import CategoriesController
from static.Contollers.ProductsController import ProductsController
from static.Contollers.MenuController import MenuController
from static.Contollers.OrderController import OrderController
from static.Contollers.PermissionController import PermissionController
from static.Contollers.FixCon import FixCon

from flask_bootstrap import Bootstrap

app = Flask(__name__)
app.debug = True
app.config[
    'SECRET_KEY'] = 'B272DD7CA1756762BA722280E9B9EF81FCCA8C28AFE6F884E7D7C3E195E50F6A26E42DB41306B5FB371734CC45494623FC8DD2AE0C099654B43628C950E8C605'
app.config['SQLALCHEMY_DATABASE_URI'] = con()
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)
Bootstrap(app)

from static.Entity.Category import Category
from static.Entity.Products import Products
from static.Entity.Provider import Provider
from static.Entity.User import User
from static.Entity.Client import Client
from static.Entity.Role import Role
from static.Entity.Permission import Permission
from static.Entity.Action import Action
from static.Entity.Log import Log
from static.Entity.Orders import Orders
from static.Entity.Menu import Menu
from static.Entity.Comments import Comments
from static.Entity.Notification import Notification

"""
    Регистрация контроллеров
"""
SecurityController.register(app, route_base="/api")
UserController.register(app, route_base='/api/user')
RoleController.register(app, route_base='/api/role')
Clientcontroller.register(app, route_base='/api/client')
CategoryController.register(app, route_base='/api/category')
OrdersController.register(app, route_base='/api/order')
CommentController.register(app, route_base='/api/comment')
ProductController.register(app, route_base='/api/product')
IndexController.register(app, route_base='/')
RolesController.register(app, route_base='/role')
ClientsController.register(app, route_base='/client')
ProvidersController.register(app, route_base='/provider')
CategoriesController.register(app, route_base='/category')
ProductsController.register(app, route_base='/product')
MenuController.register(app, route_base='/menu')
OrderController.register(app, route_base='/order')
PermissionController.register(app, route_base='/permission')
FixCon.register(app, route_base='/fix')

app.jinja_env.globals.update(getmenu=MenuController.getmenu)


def datetimeformat(value, format='%d.%m.%Y'):
    return value.strftime(format)

app.jinja_env.filters['datetime'] = datetimeformat


if __name__ == '__main__':
    app.run(host='0.0.0.0')

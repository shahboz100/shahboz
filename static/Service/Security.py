import jwt
from functools import wraps
from flask import request, jsonify, session


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'access-token' in request.headers:
            token = request.headers['access-token']

        if not token:
            return jsonify({'message': 'Token is missing!'}), 401

        try:
            from static.Connection import config
            from static.Entity.User import User
            data = jwt.decode(token, config())
            current_user = User.query.filter_by(login=data['login']).first()
        except:
            return jsonify({'message': 'Token is invalid!'}), 401

        return f(*args, current_user, **kwargs)

    return decorated


def is_granted(module):
    if session.get('id'):
        from static.Entity.User import User
        user = User.query.filter_by(id=session['id']).first()

        if 'ROLE_ADMIN' in user.role.name:
            return True
        from app import db
        rp = [c.permission for c in db.engine.execute(
            "SELECT rp.role_id, rp.permission_id, " +
            "(SELECT name FROM role r WHERE r.id = rp.role_id) role_name, " +
            "(SELECT name FROM permission p WHERE p.id = rp.permission_id) permission " +
            "from permissions_roles rp;")]
        if module in rp:
            return True
        else:
            return False
    return False


def sendNotification(user_id):
    import requests
    from flask import json
    from static.Entity.User import User
    from static.Entity.Notification import Notification
    user = User.query.filter_by(id=user_id).first()
    notification = Notification.query.filter_by(id_user=user.id).first()
    key = "key=AAAAFznKdg4:APA91bFJcLAr9I92k6OXVsdHBatYTUCRCaosGbqiFM0IM5zv1-5Cn3cjx31Uo-" \
          "GpCHaNWPq9USYa-EIV89ZK7FTQo0e5d-advFoxdxawLy304QoKzIXgfeHRK4bEqZY0CITajOwNlyYG"
    if notification is not None:
        data = {
            "to": notification.name,
            "notification": {
                "title": "Новый заказ",
                "body": "У вас новый заказ!",
                "sound": "default"
            }
        }
        headers = {"Authorization": key,
                   "Content-Type": "application/json"}

        requests.post('https://fcm.googleapis.com/fcm/send', headers=headers, data=json.dumps(data))

        return "success"
    else:
        return "not success"


def sendNotificationEdit(user_id):
    import requests
    from flask import json
    from static.Entity.User import User
    from static.Entity.Notification import Notification
    user = User.query.filter_by(id=user_id).first()
    notification = Notification.query.filter_by(id_user=user.id).first()
    key = "key=AAAAFznKdg4:APA91bFJcLAr9I92k6OXVsdHBatYTUCRCaosGbqiFM0IM5zv1-5Cn3cjx31Uo-" \
          "GpCHaNWPq9USYa-EIV89ZK7FTQo0e5d-advFoxdxawLy304QoKzIXgfeHRK4bEqZY0CITajOwNlyYG"
    if notification is not None:
        data = {
            "to": notification.name,
            "notification": {
                "title": "Изменения в заказе",
                "body": "Клиент изменил параметры заказа!",
                "sound": "default"
            }
        }
        headers = {"Authorization": key,
                   "Content-Type": "application/json"}

        requests.post('https://fcm.googleapis.com/fcm/send', headers=headers, data=json.dumps(data))

        return "success"
    else:
        return "not success"

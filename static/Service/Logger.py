import datetime
from flask import session


def Logger(action):

    from app import db
    from static.Entity.Log import Log
    from static.Entity.Action import Action
    from static.Entity.User import User
    id_action = Action.query.filter_by(name=action).first()
    user = User.query.filter_by(id=session['id']).first()
    if not id_action:
        new_action = Action(name=action)
        db.session.add(new_action)
        db.session.commit()
        id_action = Action.query.filter_by(name=action).first()
    newlog = Log(id_user=user.id, id_action=id_action.id, date=datetime.datetime.now())

    db.session.add(newlog)
    db.session.commit()

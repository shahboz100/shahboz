from app import db


class Orders(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    id_product = db.Column(db.Integer, db.ForeignKey("products.id"))
    id_client = db.Column(db.Integer, db.ForeignKey("client.id"))
    id_provider = db.Column(db.Integer, db.ForeignKey("provider.id"))
    date = db.Column(db.DateTime)
    create_date = db.Column(db.DateTime)
    count = db.Column(db.Integer)
    comment = db.Column(db.String)
    confirm = db.Column(db.Boolean)
    accepted = db.Column(db.Boolean)

    provider = db.relationship("Provider", back_populates="orders")
    product = db.relationship("Products", back_populates="orders")
    client = db.relationship("Client", back_populates="orders")
    comments = db.relationship("Comments", back_populates="orders", cascade="delete")

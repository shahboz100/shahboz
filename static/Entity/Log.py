from app import db


class Log(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    id_user = db.Column(db.Integer, db.ForeignKey("user.id"))
    id_action = db.Column(db.Integer, db.ForeignKey("action.id"))
    date = db.Column(db.DateTime)

    user = db.relationship("User", cascade="delete")
    action = db.relationship("Action")

    def __repr__(self):
        return self.username
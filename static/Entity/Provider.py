from app import db

provider_products = db.Table("provider_products",
                             db.Column("provider_id", db.Integer, db.ForeignKey("provider.id")),
                             db.Column("product_id", db.Integer, db.ForeignKey("products.id"))
                             )


class Provider(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    id_user = db.Column(db.Integer, db.ForeignKey("user.id"))
    organization = db.Column(db.String(255))
    address = db.Column(db.String(255), unique=True)
    products = db.relationship("Products",
                               secondary=provider_products,
                               back_populates="providers")

    user = db.relationship("User", back_populates="provider")

    orders = db.relationship("Orders", back_populates="provider")
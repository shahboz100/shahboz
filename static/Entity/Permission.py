from app import db
from static.Entity.Role import permissions_roles


class Permission(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(100))

    roles = db.relationship("Role",
                            secondary=permissions_roles,
                            back_populates="permissions")

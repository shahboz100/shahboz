from app import db


class Notification(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(255))
    id_user = db.Column(db.Integer, db.ForeignKey("user.id"))

    user = db.relationship("User")

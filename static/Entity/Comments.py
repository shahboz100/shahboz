from app import db


class Comments(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    id_user = db.Column(db.Integer, db.ForeignKey("user.id"))
    id_order = db.Column(db.Integer, db.ForeignKey("orders.id"))
    text = db.Column(db.Text)
    date = db.Column(db.DateTime)

    user = db.relationship("User", back_populates="comments")
    orders = db.relationship("Orders", back_populates="comments")
from app import db


class Action(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(255), unique=True)

    def __repr__(self):
        return self.name
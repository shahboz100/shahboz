from app import db


class Menu(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(100))
    route = db.Column(db.String(255))
    id_role = db.Column(db.Integer, db.ForeignKey("role.id"))
    order = db.Column(db.Integer)

    role = db.relationship("Role")

    def __repr__(self):
        return self.name
from app import db

permissions_roles = db.Table("permissions_roles",
                             db.Column("role_id", db.Integer, db.ForeignKey("role.id")),
                             db.Column("permission_id", db.Integer, db.ForeignKey("permission.id"))
                             )


class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(255), unique=True)

    user = db.relationship("User", back_populates="role")
    permissions = db.relationship("Permission",
                                  secondary=permissions_roles,
                                  back_populates="roles", cascade="save-update, merge, delete")
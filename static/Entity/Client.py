from app import db


class Client(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name_org = db.Column(db.String(255))
    address = db.Column(db.String(255), unique=True)
    id_user = db.Column(db.Integer, db.ForeignKey("user.id"))

    user = db.relationship("User", back_populates="client", cascade="save-update, merge, delete")
    orders = db.relationship("Orders", back_populates="client", cascade="save-update, merge, delete")

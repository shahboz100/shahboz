from app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    login = db.Column(db.String(80), unique=True)
    password = db.Column(db.String)
    salt = db.Column(db.String)
    fio = db.Column(db.String(255))
    email = db.Column(db.String(120), unique=True)
    phone = db.Column(db.String(12), unique=True)
    id_role = db.Column(db.Integer, db.ForeignKey("role.id"))
    status = db.Column(db.Boolean)

    role = db.relationship("Role", back_populates="user", cascade="delete")
    provider = db.relationship("Provider", back_populates="user", cascade="delete")
    client = db.relationship("Client", back_populates="user", cascade="delete")
    comments = db.relationship("Comments", back_populates="user", cascade="delete")

    def __init__(self, login, password, salt, fio, email, phone, id_role, status):
        self.login = login
        self.password = password
        self.salt=salt
        self.fio = fio
        self.email = email
        self.phone = phone
        self.id_role = id_role
        self.status=status

    def __repr__(self):
        return self.fio



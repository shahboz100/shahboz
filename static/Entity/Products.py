from app import db
from static.Entity.Provider import provider_products


class Products(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(255))
    code = db.Column(db.Integer, unique=True)
    price = db.Column(db.Integer)
    count = db.Column(db.Integer)
    unit = db.Column(db.String)
    id_category = db.Column(db.Integer, db.ForeignKey("category.id"))
    providers = db.relationship("Provider",
                                secondary=provider_products,
                                back_populates="products")
    category = db.relationship("Category")
    orders = db.relationship("Orders", back_populates="product", cascade="delete")
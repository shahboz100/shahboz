# -*- coding: utf-8 -*-
from flask import render_template, redirect, session
from flask_classy import FlaskView, route
from static.Service.Logger import Logger


class ProductsController(FlaskView):

    MODULE_NAME = 'PRODUCTS'

    VIEW = MODULE_NAME + '_VIEW'
    UPDATE = MODULE_NAME + '_UPDATE'
    DELETE = MODULE_NAME + '_DELETE'

    """
        Список товаров 
    """
    @route('/')
    def index(self):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):
            from static.Entity.Products import Products
            from static.Entity.User import User
            from static.Entity.Provider import Provider

            user = User.query.filter_by(id=session['id']).first()
            if user.role.name == 'ROLE_PROVIDER':
                prov = Provider.query.filter_by(id_user=user.id).first()
                products = Products.query.filter(Products.providers.any(id=prov.id)).all()
            else:
                products = Products.query.all()

            Logger('Просмотр списка товаров')

            return render_template('admin/product/index.html', title='Список товаров', products=products)
        return redirect('/')

    """
        Добавление товара
    """
    @route('/add', methods=['GET', 'POST'])
    def add(self):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Category import Category
            from app import db
            categories = [(c.id, c.name) for c in Category.query.all()]
            provider = [(c.id, c.fio) for c in db.engine.execute(
                "SELECT p.id, u.fio, u.phone, p.organization, p.address FROM " + '"user"' + " u JOIN provider p ON u.id = p.id_user;")]
            from static.Form.ProductType import ProductType
            form = ProductType()
            form.category.choices = categories
            form.provider.choices = provider
            if form.validate_on_submit():
                from static.Entity.Products import Products
                new_product = Products(name=form.name.data,
                                       code=form.code.data,
                                       price=form.price.data,
                                       count=form.count.data,
                                       id_category=form.category.data, unit=form.unit.data)
                from static.Entity.Provider import Provider
                prov = Provider.query.filter_by(id=form.provider.data).first()
                prov.products.append(new_product)
                db.session.add(new_product)
                db.session.add(prov)
                db.session.commit()

                Logger('Добавление товара')

                return redirect('/product')
            return render_template('admin/product/add.html', title='Добавление товара', form=form)
        return redirect('/')

    """
        Изменеие товара
    """
    @route('/edit/<int:id_product>', methods=['GET', 'POST'])
    def edit(self, id_product):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Category import Category
            from static.Entity.Products import Products
            from app import db
            categories = [(c.id, c.name) for c in Category.query.all()]
            provider = [(c.id, c.fio) for c in db.engine.execute(
                "SELECT p.id, u.fio, u.phone, p.organization, p.address FROM " + '"user"' + " u JOIN provider p ON u.id = p.id_user;")]
            product = Products.query.filter_by(id=id_product).first()
            from static.Form.ProductEditType import ProductType
            form = ProductType()
            form.category.choices = categories

            if form.validate_on_submit():

                product.name = form.name.data
                product.code = form.code.data
                product.price = form.price.data
                product.count = form.count.data
                product.unit = form.unit.data
                product.id_category = form.category.data

                db.session.commit()

                Logger('Изменение товара')

                return redirect('/product')
            else:
                form.name.data = product.name
                form.code.data = product.code
                form.price.data = product.price
                form.count.data = product.count
                form.category.choices = categories
                form.unit.data = product.unit
            return render_template('admin/product/edit.html', title='Изменение товара', product=product, form=form)
        return redirect('/')

    """
        Просмотр товаров
    """
    @route('/show/<int:id_product>', methods=['GET'])
    def show(self, id_product):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):

            from static.Entity.Products import Products
            product = Products.query.filter_by(id=id_product).first()

            Logger('Просмотр товаров')

            return render_template('admin/product/show.html', title='Просмотр товара', product=product)
        return redirect('/')

    """
        Добавление поставщика в товары
    """
    @route('/addProvider/<int:id_product>', methods=['GET', 'POST'])
    def addProvider(self, id_product):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Products import Products
            product = Products.query.filter_by(id=id_product).first()

            from static.Form.ProductProvider import ProductProvider
            form = ProductProvider()
            from app import db
            provider = [(c.id, c.fio) for c in db.engine.execute(
                "SELECT p.id, u.fio, u.phone, p.organization, p.address FROM " + '"user"' + " u JOIN provider p ON u.id = p.id_user;")]
            form.provider.choices = provider
            if form.validate_on_submit():

                from static.Entity.Provider import Provider
                prov = Provider.query.filter_by(id=form.provider.data).first()
                prov.products.append(product)
                db.session.add(prov)
                db.session.commit()

                Logger('Связка продукта с поставщиков')

                return redirect('/product/show/'+str(product.id))

            return render_template('admin/product/addProvider.html', title='Добавление товара поставщику', form=form, product=product)
        return redirect('/')

    """
        Удаление поставщика из товаров
    """
    @route('/deleteProvider/<int:id_product>/<int:id_provider>', methods=['GET', 'POST'])
    def deleteProvider(self, id_product, id_provider):

        from static.Service.Security import is_granted
        if is_granted(self.DELETE):

            from static.Entity.Products import Products
            from static.Entity.Provider import Provider
            from app import db
            provider = Provider.query.filter_by(id=id_provider).first()
            product = Products.query.filter_by(id=id_product).first()

            provider.products.remove(product)
            db.session.commit()

            Logger('Удаление поставщика из товаров')

            return redirect('product/show/'+str(id_product))
        return redirect('/')

    """
             Удаление поставщика
    """
    @route('/delete/<int:id_product>', methods=['GET'])
    def delete(self, id_product):

        from static.Service.Security import is_granted
        if is_granted(self.DELETE):
            from static.Entity.Products import Products
            product = Products.query.filter_by(id=id_product).first()
            from app import db
            db.session.delete(product)
            db.session.commit()

            Logger('Удаление поставщика')

            return redirect('/product')
        else:
            return redirect('/')
# -*- coding: utf-8 -*-
from flask import render_template, redirect
from flask_classy import FlaskView, route
from static.Service.Logger import Logger


class PermissionController(FlaskView):
    MODULE_NAME = 'PERMISSIONS'

    VIEW = MODULE_NAME + '_VIEW'
    UPDATE = MODULE_NAME + '_UPDATE'
    DELETE = MODULE_NAME + '_DELETE'

    """
        Список разрешений
    """

    @route('/')
    def index(self):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):
            from static.Entity.Permission import Permission
            permission = Permission.query.all()

            Logger('Просмотр списка разрешений')

            return render_template('admin/permission/index.html', title='Список прав доступа', permissions=permission)
        return redirect('/')

    """
         Добавление разрешений
    """

    @route('/add', methods=['GET', 'POST'])
    def add(self):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Form.PermissionType import PermissionType
            form = PermissionType()
            if form.validate_on_submit():
                from static.Entity.Permission import Permission
                permission = Permission(name=form.name.data)
                from app import db
                db.session.add(permission)
                db.session.commit()

                Logger('Добавление  разрешений')

                return redirect('/permission')
            return render_template('admin/permission/add.html', title='Добавление права доступа', form=form)
        return redirect('/')

    """
        Изменеие разрешений
    """

    @route('/edit/<int:id_permission>', methods=['GET', 'POST'])
    def edit(self, id_permission):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Permission import Permission
            permission = Permission.query.filter_by(id=id_permission).first()

            from static.Form.PermissionType import PermissionType
            form = PermissionType(obj=permission)
            if form.validate_on_submit():
                from app import db
                permission.name = form.name.data
                db.session.commit()

                Logger('Изменение  разрешений')

                return redirect('/permission')

            return render_template('admin/permission/edit.html', title='Изменение првва доступа', permission=permission,
                                   form=form)
        return redirect('/')

    """
         Удаление разрешений
    """

    @route('/delete/<int:id_permission>', methods=['GET'])
    def delete(self, id_permission):

        from static.Service.Security import is_granted
        if is_granted(self.DELETE):

            from static.Entity.Permission import Permission
            permission = Permission.query.filter_by(id=id_permission).first()
            from app import db
            db.session.delete(permission)
            db.session.commit()

            Logger('Удаление  разрешений')

            return redirect('/permission')
        else:
            return redirect('/')

    """
        Разрешение-Роль
    """

    @route('/addroleperm', methods=['GET', 'POST'])
    def addroleperm(self):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Form.Role_PermissionType import Role_PermissionType
            from static.Entity.Role import Role
            from static.Entity.Permission import Permission
            roles = [(r.id, r.name) for r in Role.query.all()]
            permissions = [(r.id, r.name) for r in Permission.query.all()]
            form = Role_PermissionType()

            form.role.choices = roles
            form.permission.choices = permissions
            if form.validate_on_submit():
                roll = Role.query.filter_by(id=form.role.data).first()
                perm = Permission.query.filter_by(id=form.permission.data).first()
                roll.permissions.append(perm)

                from app import db
                db.session.commit()

                Logger('Привязка разрешения к роли ')

                return redirect('/permission/rolepermission')
            return render_template('admin/permission/role_perm.html', form=form, title='Добавление роли-права')
        else:
            return redirect('/')

    """
        Список разрешений-прав
    """

    @route('/rolepermission')
    def indexRP(self):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):
            from app import db
            rp = db.engine.execute(
                "SELECT rp.role_id, rp.permission_id, (SELECT name FROM role r WHERE r.id = rp.role_id) role_name, (SELECT name FROM permission p WHERE p.id = rp.permission_id) permission from permissions_roles rp;").fetchall()

            Logger('Список разрешений-правил')

            return render_template('admin/permission/rolepermissionindex.html', title='Права-Роль', roles=rp)
        return redirect('/')

    """
             Удаление разрешений
    """

    @route('/deleterp/<int:id_role>/<int:id_permission>', methods=['GET'])
    def deleteRP(self, id_role, id_permission):

        from static.Service.Security import is_granted
        if is_granted(self.DELETE):

            from static.Entity.Permission import Permission
            from static.Entity.Role import Role
            role = Role.query.filter_by(id=id_role).first()
            permission = Permission.query.filter_by(id=id_permission).first()
            from app import db
            role.permissions.remove(permission)
            db.session.commit()

            Logger('Удаление разрешений')

            return redirect('/permission/rolepermission')
        else:
            return redirect('/')

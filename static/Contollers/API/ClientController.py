# -*- coding: utf-8 -*-
from flask import request, make_response
from flask.json import jsonify
from flask_classy import FlaskView, route
from static.Service.Logger import Logger
from static.Service.Security import token_required


class Clientcontroller(FlaskView):

    """
        Список клиентов
    """
    @route('/')
    @token_required
    def role_index(self, corrent_user):

        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=corrent_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "Permission denied!"})

        from static.Entity.Client import Client
        clients = Client.query.all()
        out = []
        for client in clients:
            data = {}
            data['id'] = client.id
            data['name_org'] = client.name_org
            data['address'] = client.address
            data['id_user'] = client.id_user
            out.append(data)

        return jsonify(out)

    """
        Возвращает одного клиента 
    """
    @route('/<int:id_c>', methods=['GET'])
    @token_required
    def get_one_client(self, current_user, id_c):

        from static.Entity.Client import Client

        client = Client.query.filter_by(id=id_c).first()

        if not client:
            return jsonify({'message': 'No user found!'})
        else:
            Logger(current_user, 'Просмотр клиента')

        data = {}
        data['id'] = client.id
        data['name_org'] = client.name_org
        data['address'] = client.address
        data['id_user'] = client.id_user

        return jsonify({'client': data})

    """
        Добавление клиента 
    """
    @route('/add/<int:id_user>', methods=['POST','GET'])
    @token_required
    def client_add(self, corrent_user, id_user):
        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=corrent_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "Permission denied!"})
        from static.Entity.Client import Client
        from static.Entity.User import User
        user = User.query.filter_by(id=id_user).first()
        data = request.json

        new_client = Client(name_org=data['name_org'], address=data['address'], id_user=user.id)
        from app import db
        db.session.add(new_client)
        db.session.commit()

        Logger(corrent_user,'Добавление нового клиента')

        return jsonify({"message": "New client created"})

    """
        Изменение клиента
    """
    @route('/edit', methods=['PUT'])
    @token_required
    def client_edit(self, corrent_user):

        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=corrent_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "Permission denied!"})
        from static.Entity.Client import Client

        data = request.json
        client = Client.query.filter_by(id=data['id']).first()
        client.name_org = data['name_org']
        client.address = data['address']

        from app import db
        db.session.commit()

        Logger(corrent_user, 'Изменение клиента')

        return jsonify({"message": "Edit client success!"})

    """
        Удаление клиента
    """
    @route('/delete', methods=['DELETE'])
    @token_required
    def client_delete(self, corrent_user):
        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=corrent_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "Permission denied!"})
        data=request.json
        from static.Entity.Client import Client
        client = Client.query.filter_by(id=data['id']).first()
        if not client:
            return jsonify({"message": "Client not found"})
        from app import db
        db.session.delete(client)
        db.session.commit()

        Logger(corrent_user, 'Удаление клиента')

        return jsonify({"message": "Client deleted"})
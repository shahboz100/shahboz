# -*- coding: utf-8 -*-
from flask import request, make_response
from flask.json import jsonify
from flask_classy import FlaskView, route
from static.Service.Logger import Logger
from static.Service.Security import token_required


class CategoryController(FlaskView):

    """
        Возвращает все категории
    """
    @route('/')
    @token_required
    def category_index(self, corrent_user):

        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=corrent_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "Permission denied!"})
        from static.Entity.Category import Category
        category = Category.query.all()
        out = []
        for cate in category:
            data = {}
            data['id'] = cate.id
            data['name'] = cate.name
            out.append(data)

        Logger(corrent_user, 'Просмотр всех категорий')

        return jsonify(out)

    """
            Добавление категории 
    """
    @route('/add', methods=['POST'])
    @token_required
    def category_add(self, corrent_user):

        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=corrent_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "Permission denied!"})

        from static.Entity.Category import Category
        data = request.json

        new_category = Category(name=data['name'])
        from app import db
        db.session.add(new_category)
        db.session.commit()

        Logger(corrent_user, 'Добавление новой категории')

        return jsonify({"message": "Новая категория добавлена"})

    """
            Изменение категории
    """
    @route('/edit', methods=['PUT'])
    @token_required
    def category_edit(self, corrent_user):

        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=corrent_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "Permission denied!"})

        from static.Entity.Category import Category
        data = request.json
        category = Category.query.filter_by(id=data['id']).first()
        category.name = data['name']

        from app import db
        db.session.commit()

        Logger(corrent_user, 'Изменение категории')

        return jsonify({"message": "Категория изменена"})

    """
           Удаление категории
    """
    @route('/delete', methods=['DELETE'])
    @token_required
    def category_delete(self, corrent_user):
        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=corrent_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "Permission denied!"})

        data = request.json
        from static.Entity.Category import Category
        category = Category.query.filter_by(id=data['id']).first()
        if not category:
            return jsonify({"message": "Категория не найдена"})
        from app import db
        db.session.delete(category)
        db.session.commit()

        Logger(corrent_user, 'Удаление категории')

        return jsonify({"message": "Категория удалена"})
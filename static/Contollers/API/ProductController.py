# -*- coding: utf-8 -*-
from flask.json import jsonify
from flask_classy import FlaskView, route
from static.Service.Security import token_required


class ProductController(FlaskView):
    """
            Возвращает все товары
    """

    @route('/')
    @token_required
    def role_index(self, current_user):

        from static.Entity.Products import Products
        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=current_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "У вас нет доступа к просмотру данного раздела"})

        products = Products.query.order_by(Products.count.asc()).all()
        out = []
        for product in products:
            data = {'id': product.id, 'name': product.name, 'price': product.price, 'count': product.count,
                    'category': product.category.name}
            out.append(data)

        return jsonify({'products': out})

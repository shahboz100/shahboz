from flask import request
from flask.json import jsonify
from flask_classy import FlaskView, route
from static.Service.Security import token_required
import datetime
from datetime import datetime


class CommentController(FlaskView):
    """
        Возвращает все комментарии
    """

    @route('/<int:product>')
    @token_required
    def comment_index(self, current_user, product):

        from static.Entity.Comments import Comments

        comments = Comments.query.filter_by(id_order=product).all()

        out = []
        for comment in comments:
            data = {'id': comment.id,
                    'author': comment.user.fio,
                    'comment': comment.text,
                    'id_order': comment.id_order,
                    'date': datetime.strftime(comment.date, "%d.%m.%Y"),
                    'time': datetime.strftime(comment.date, "%H:%M:%S"),
                    }
            out.append(data)

        return jsonify({"comments": out})

    """
        Добавление коментария
    """
    @route('/add/<int:product>', methods=["POST"])
    @token_required
    def setConmment(self, current_user, product):

        from static.Entity.Comments import Comments
        data = request.json

        row = data['comment']
        r = ""

        for i in range(2, len(row) - 2):
            r += row[i]

        new_comment = Comments(id_user=current_user.id, id_order=product, text=r, date=datetime.today())

        from app import db
        db.session.add(new_comment)
        db.session.commit()

        return jsonify({"success": True})

    """
            Удаление коментария
    """
    @route('/delete/<int:comment_id>', methods=["POST"])
    @token_required
    def deleteConmment(self, current_user, comment_id):

        from static.Entity.Comments import Comments

        comment = Comments.query.filter_by(id=comment_id).first()

        if not comment:
            return jsonify({"message": "Коментарий не найден"})

        if comment.user != current_user:
            return jsonify({"message": "Вы не можете удалить чужой коментарий"})


        from app import db

        db.session.delete(comment)
        db.session.commit()

        return jsonify({"success": True})
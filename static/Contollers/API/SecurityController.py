# -*- coding: utf-8 -*-
import datetime
import jwt
from flask import request, session
from flask.json import jsonify
from flask_classy import FlaskView, route
from werkzeug.security import check_password_hash
from static.Service.Logger import Logger


class SecurityController(FlaskView):
    """
        Авторизация пользователя.
        Выдача токена
    """

    @route('/login', methods=['POST'])
    def login(self):
        auth = request.authorization

        if not auth or not auth.username or not auth.password:
            return jsonify({'error': 'Вы ввели не верный пароль или логин'})

        from static.Entity.User import User
        user = User.query.filter_by(login=auth.username).first()

        if not user:
            return jsonify({'error': 'Пользователь с таким идентификатором не найден'})

        if check_password_hash(user.password, auth.password):
            from app import app
            if user.status:
                session['id'] = user.id
                session['login'] = user.login

                token = jwt.encode(
                    {'login': user.login, 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30)},
                    app.config['SECRET_KEY'])

                Logger('Вход на сайт API')

                return jsonify({'token': token.decode('UTF-8'), 'role': user.role.name})
            else:
                return jsonify({'error': 'Администратор еще не проверил ваш аккаунт'})

        return jsonify({'error': 'Вы ввели не верный пароль или логин'})
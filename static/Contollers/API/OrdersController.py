# -*- coding: utf-8 -*-
import datetime

from flask.json import jsonify
from flask import render_template, redirect, session
from flask_classy import FlaskView, route
from static.Service.Logger import Logger
from datetime import datetime
from static.Service.Security import token_required


class OrdersController(FlaskView):
    MODULE_NAME = 'ORDERS'

    VIEW = MODULE_NAME + '_VIEW'
    MY_VIEW = MODULE_NAME + '_MY_VIEW'
    UPDATE = MODULE_NAME + '_UPDATE'
    DELETE = MODULE_NAME + '_DELETE'
    CONFIRM = MODULE_NAME + '_CONFIRM'

    """
            Список заказов
    """

    @route('/', methods=['GET', 'POST'])
    @token_required
    def index(self, current_user):

        from static.Entity.Orders import Orders
        from static.Entity.Provider import Provider
        from static.Entity.User import User
        user = User.query.filter_by(id=current_user.id).first()
        provider = Provider.query.filter_by(id_user=user.id).first()
        if provider is not None:
            orders = Orders.query.filter_by(id_provider=provider.id).all()
        else:
            orders = Orders.query.all()

        output = []

        for order in orders:
            order_data = {'id': order.id,
                          'product': order.product.name,
                          'provider': order.provider.user.fio,
                          'date': datetime.strftime(order.date, "%d.%m.%Y"),
                          'createDate': datetime.strftime(order.create_date, "%d.%m.%Y"),
                          'count': order.count,
                          'comment': order.comment,
                          'confirm': order.confirm,
                          'client': order.client.user.fio}

            output.append(order_data)
        return jsonify({'orders': output})

    """
                Список заказов поставщика
    """

    @route('/my', methods=["GET"])
    @token_required
    def my_orders(self, current_user):

        from static.Entity.Orders import Orders
        from static.Entity.User import User
        from static.Entity.Provider import Provider
        user = User.query.filter_by(id=current_user.id).first()
        provider = Provider.query.filter_by(id_user=user.id).first()
        orders = Orders.query.filter_by(id_provider=provider.id).all()

        output = []

        for order in orders:
            order_data = {'id': order.id,
                          'product': order.product.name,
                          'provider': order.provider.user.fio,
                          'date': datetime.strftime(order.date, "%d.%m.%Y"),
                          'createDate': datetime.strftime(order.create_date, "%d.%m.%Y"),
                          'count': order.count,
                          'comment': order.comment,
                          'confirm': order.confirm,
                          'client': order.client.user.fio}

            output.append(order_data)
        return jsonify({'orders': output})

    """
        Подтверждение заказа
    """

    @route('/confirm/<int:id_order>', methods=['GET'])
    @token_required
    def confirm(self, current_user, id_order):

        from static.Entity.Orders import Orders
        from static.Entity.User import User
        from static.Entity.Provider import Provider

        user = User.query.filter_by(id=current_user.id).first()
        provider = Provider.query.filter_by(id_user=user.id).first()

        if not provider:
            return jsonify({'message': 'Вы не можете подтвердить заказ!'})
        order = Orders.query.filter_by(id=id_order).first()
        print(order.provider.id)
        print(provider.id)
        if order.provider.id != provider.id:

            return jsonify({'message': 'Вы не можете подтвердить заказ!'})

        order.confirm = 1
        from app import db
        db.session.commit()

        return jsonify({'success': True})

# -*- coding: utf-8 -*-
import datetime

from flask.json import jsonify, request
from flask_classy import FlaskView, route
from static.Service.Security import token_required
from static.Service.Logger import Logger
from werkzeug.security import generate_password_hash


class UserController(FlaskView):
    """
        Возвращает всех пользователей из таблицы User
    """

    @route('/', methods=['GET'])
    @token_required
    def get_all_users(self):
        Logger('Просмотр всех пользователей')
        from static.Entity.User import User
        users = User.query.all()

        output = []

        for user in users:
            user_data = {'id': user.id, 'login': user.login, 'password': user.password}
            output.append(user_data)

        return jsonify({'users': output})

    """
        Возвращает профильные данные пользователя исходя из токена
    """

    @route('/profile', methods=['GET'])
    @token_required
    def get_one_user(self, current_user):

        if not current_user:
            return jsonify({'message': 'Пользователь не найден!'})

        user_data = {'id': current_user.id, 'login': current_user.login, 'fio': current_user.fio,
                     'email': current_user.email,
                     'phone': current_user.phone, 'role': current_user.role.name, 'status': current_user.status}

        return jsonify({'user': user_data})

    """
        Регистрация пользовател
    """

    @route('/register', methods=['POST'])
    def create_user(self):
        data = request.json
        from static.Entity.User import User
        from static.Entity.Role import Role

        if User.query.filter_by(login=data['login']).first() is not None:
            return jsonify({'message': 'Такой логин уже существует в системе!'})
        if User.query.filter_by(email=data['email']).first() is not None:
            return jsonify({'message': 'Пользователь с таким email существует в системе!'})
        if User.query.filter_by(phone=data['phone']).first() is not None:
            return jsonify({'message': 'Пользователь с таким телефоном существует в системе!'})

        salt = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
        hashed_password = generate_password_hash(data['password'], method='sha256')
        str_role = data['provider']
        from static.Entity.Provider import Provider
        from static.Entity.Client import Client
        if str_role == 1:

            if Provider.query.filter_by(address=data['address']).first() is not None:
                return jsonify({'message': 'Пользователь с таким адресом существует в системе!'})

            role = Role.query.filter_by(name='ROLE_PROVIDER').first()
        else:

            if Client.query.filter_by(address=data['address']).first() is not None:
                return jsonify({'message': 'Пользователь с таким адресом существует в системе!'})

            role = Role.query.filter_by(name='ROLE_CLIENT').first()

        new_user = User(login=data['login'], password=hashed_password, salt=salt, fio=data['fio'],
                        email=data['email'],
                        phone=data['phone'], id_role=role.id, status=0)
        from app import db

        db.session.add(new_user)
        db.session.commit()

        if str_role == 1:
            new_provider = Provider(id_user=new_user.id, organization=data['organization'], address=data['address'])
            db.session.add(new_provider)
            db.session.commit()
        else:
            new_client = Client(id_user=new_user.id, name_org=data['organization'], address=data['address'])
            db.session.add(new_client)
            db.session.commit()

        return jsonify({'status': True})

    @route("/notification", methods=["POST"])
    @token_required
    def notification(self, current_user):
        data = request.json
        from app import db
        from static.Entity.Notification import Notification
        notification = Notification.query.filter_by(id_user=current_user.id).first()
        if notification is None:
            notification = Notification(name=data['key'], id_user=current_user.id)
            db.session.add(notification)
        else:
            notification.id_user = current_user.id
            notification.name = data['key']
        db.session.commit()

        return jsonify({"success": True})


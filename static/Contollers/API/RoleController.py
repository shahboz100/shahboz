# -*- coding: utf-8 -*-
from flask import request, make_response
from flask.json import jsonify
from flask_classy import FlaskView, route
from static.Service.Logger import Logger
from static.Service.Security import token_required


class RoleController(FlaskView):

    """
        Возвращает все роли
    """
    @route('/')
    @token_required
    def role_index(self, corrent_user):

        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=corrent_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "Permission denied!"})

        roles = Role.query.all()
        out = []
        for role in roles:
            data = {}
            data['id'] = role.id
            data['name'] = role.name
            out.append(data)

        Logger(corrent_user, 'Просмотр всех ролей')

        return jsonify(out)

    """
        Добавление новой роли
    """
    @route('/add', methods=['POST'])
    @token_required
    def role_add(self, corrent_user):
        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=corrent_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "Permission denied!"})

        data = request.json
        new_role = Role(name=data['name'])
        from app import db
        db.session.add(new_role)
        db.session.commit()

        Logger(corrent_user,'Добавление новой роли')

        return jsonify({"message": "New role created"})

    """
        Изменение роли
    """
    @route('/edit/<int:id_role>', methods=['PUT'])
    @token_required
    def role_edit(self, corrent_user, id_role):
        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=corrent_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "Permission denied!"})
        role = Role.query.filter_by(id=id_role).first()
        data = request.json
        role.name = data['name']

        from app import db
        db.session.commit()

        Logger(corrent_user, 'Изменение роли')

        return jsonify({"message": "Edit role success!"})

    """
        Удаление роли
    """
    @route('/delete/<int:id_role>', methods=['DELETE'])
    @token_required
    def role_add(self, corrent_user, id_role):
        from static.Entity.Role import Role
        role_user = Role.query.filter_by(id=corrent_user.id_role).first()

        if role_user.name != 'ROLE_ADMIN':
            return jsonify({"message": "Permission denied!"})
        role = Role.query.filter_by(id=id_role).first()
        if not role:
            return jsonify({"message": "Role not found"})
        from app import db
        db.session.delete(role)
        db.session.commit()

        Logger(corrent_user, 'Удаление роли')

        return jsonify({"message": "Role deleted"})

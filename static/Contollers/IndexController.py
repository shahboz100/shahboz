# -*- coding: utf-8 -*-
import datetime

from flask import request, render_template, redirect, session
from flask_classy import FlaskView, route
from werkzeug.security import check_password_hash, generate_password_hash
from static.Service.Logger import Logger
from static.Form.LoginType import LoginType


class IndexController(FlaskView):
    MODULE_NAME = 'CONFIRM'

    VIEW = MODULE_NAME + '_VIEW'
    UPDATE = MODULE_NAME + '_UPDATE'

    """
        Главная страница
    """

    @route('/', methods=['GET', 'POST'])
    def index(self):

        form = LoginType()
        if 'login' in session:
            return redirect('/profile')
        else:
            if request.method == 'POST':
                if form.validate_on_submit():
                    login = form.login.data
                    password = form.password.data

                    from static.Entity.User import User
                    user = User.query.filter_by(login=login).first()

                    if not user:
                        error = 'Пользователь с таким идентификатором или паролем не найден'

                        return render_template('Index.html', title='Главная', form=form, error=error)

                    if check_password_hash(user.password, password):
                        if user.status:
                            session['id'] = user.id
                            session['login'] = user.login

                            Logger('Вход на сайт')

                            return redirect('/profile')
                        else:
                            return render_template('errorstatus.html')

                    return render_template('Index.html', title='Главная', form=form, error='Неверный логин или пароль')
        return render_template('Index.html', title='Главная', form=form, error=None)

    """
        Главная страница
    """

    @route('/logout')
    def logout(self):

        session.pop('id', None)
        session.pop('login', None)

        return redirect('/')

    """
        Профиль
    """

    @route('/profile')
    def profile(self):

        if session.get('id'):
            from static.Entity.User import User
            user = User.query.filter_by(id=session['id']).first()
            from app import db
            if user.provider:
                user = db.engine.execute(
                    "SELECT u.id, u.login, u.fio, u.email, u.phone, (SELECT name FROM role r  WHERE r.id = u.id_role) id_role, p.organization, p.address FROM " + '"user"' + "u JOIN provider p ON u.id = p.id_user WHERE u.id=" + str(
                        user.id)).fetchall()
                user = user[0]
            elif user.client:
                user = db.engine.execute(
                    "SELECT u.id, u.login, u.fio, u.email, u.phone, (SELECT name FROM role r  WHERE r.id = u.id_role) id_role, p.name_org, p.address FROM" + '"user"' + "u JOIN client p ON u.id = p.id_user where u.id=" + str(
                        user.id)).fetchall()
                user = user[0]
            Logger('Посещение профиля')

            return render_template('profile.html', user=user)

        return redirect('/')

    """
        Регистрация
    """

    @route('/registration', methods=['GET', 'POST'])
    def registration(self):

        from static.Form.RegistrationType import RegistrationType
        form = RegistrationType()
        form.is_client.choices = [(1, 'Я клиент'), (2, 'Я поставщик')]
        from static.Entity.Role import Role
        from static.Entity.User import User
        from app import db
        if form.validate_on_submit():
            if form.is_client.data == 1:

                salt1 = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
                hashed_password = generate_password_hash(form.password.data, method='sha256')
                role = "ROLE_CLIENT"
                new_user = User(login=form.login.data, password=hashed_password, salt=salt1, fio=form.fio.data,
                                email=form.email.data, phone=form.phone.data,
                                id_role=Role.query.filter_by(name=role).first().id, status=0)

                db.session.add(new_user)
                db.session.commit()
                user = User.query.filter_by(login=new_user.login).first()
                from static.Entity.Client import Client
                new_client = Client(id_user=user.id, name_org=form.organization.data, address=form.address.data)

                db.session.add(new_client)
                db.session.commit()

                return redirect('/')
            elif form.is_client.data == 2:

                salt1 = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
                hashed_password = generate_password_hash(form.password.data, method='sha256')
                role = "ROLE_PROVIDER"
                new_user = User(login=form.login.data, password=hashed_password, salt=salt1, fio=form.fio.data,
                                email=form.email.data, phone=form.phone.data,
                                id_role=Role.query.filter_by(name=role).first().id, status=0)
                db.session.add(new_user)
                db.session.commit()
                user = User.query.filter_by(login=new_user.login).first()
                from static.Entity.Provider import Provider
                new_client = Provider(id_user=user.id, organization=form.organization.data, address=form.address.data)

                db.session.add(new_client)
                db.session.commit()

                return redirect('/')
        return render_template('registration.html', form=form)

    """
        Подтверждение регистрации
    """

    @route('/confirm', methods=['GET'])
    @route('/confirm/<int:id_user>', methods=['GET'])
    def confirm(self, id_user=None):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):
            from static.Entity.User import User
            users = User.query.filter_by(status=False).all()
            if id_user:
                from app import db
                user = User.query.filter_by(id=id_user).first()
                user.status = 1

                db.session.commit()

                Logger('Подтверждение регистрации пользователя ' + str(user.fio))

                return redirect('/confirm')

            Logger('Просмотр не подтвержденных пользователей')

            return render_template('admin/confirm.html', title='Подтверждение регистрации', users=users)
        return redirect('/')

    @route('/statistic/', methods=['GET'])
    def statistic(self):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):
            from static.Entity.Provider import Provider
            from static.Entity.Client import Client
            from static.Entity.Products import Products
            from static.Entity.Orders import Orders
            from app import db
            from sqlalchemy import func
            providers = db.session.query(func.count(Provider.id)).scalar()
            clients = db.session.query(func.count(Client.id)).scalar()
            products = db.session.query(func.count(Products.id)).scalar()
            orders = db.session.query(func.count(Orders.id)).scalar()
            done_orders = db.session.query(func.count(Orders.accepted)).scalar()

            ten_products = db.session.query(Products).filter(Products.count<10)

            return render_template('statistic.html', providers=providers,
                                   clients=clients,
                                   products=products,
                                   orders=orders,
                                   done_orders=done_orders,
                                   ten_products=ten_products)
        return redirect("/")

# -*- coding: utf-8 -*-
from flask import request, make_response, render_template, redirect, session
from flask.json import jsonify
from flask_classy import FlaskView, route
from static.Service.Logger import Logger
from static.Service.Security import token_required
from static.Form.MenuType import MenuType


class MenuController(FlaskView):

    MODULE_NAME = 'MENU'

    VIEW = MODULE_NAME + '_VIEW'
    UPDATE = MODULE_NAME + '_UPDATE'
    DELETE = MODULE_NAME + '_DELETE'

    """
         Меню для шапки
    """
    def getmenu(self):

        from static.Service.Security import is_granted

        from static.Entity.User import User
        user = User.query.filter_by(id=session['id']).first()
        from app import db
        menus = db.engine.execute("SELECT * FROM menu m WHERE m.id_role="+str(user.id_role)+" order by "+'"order"'+ " asc;")

        return menus

    """
        Меню
    """

    @route('/', methods=['GET', 'POST'])
    def index(self):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):

            from static.Entity.Menu import Menu
            from sqlalchemy import asc
            menus = Menu.query.order_by(asc(Menu.order)).all()

            Logger('Просмотр списка меню')

            return render_template('admin/menu/index.html', title='Роли', menus=menus)
        else:
            return redirect('/')

    """
        Добавление элемента меню
    """

    @route('/add', methods=['GET', 'POST'])
    def add(self):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Menu import Menu
            from app import db
            from static.Entity.Role import Role
            roles = [(r.id, r.name) for r in Role.query.all()]
            form = MenuType()
            form.role.choices=roles
            if form.validate_on_submit():
                new_menu = Menu(name=form.name.data, route=form.route.data, role=Role().query.filter_by(id=form.role.data).first(), order=form.order.data)

                db.session.add(new_menu)
                db.session.commit()

                Logger('Добавление элемента меню')

                return redirect('/menu')

            return render_template('admin/menu/add.html', title='Добавление элемента меню', form=form)
        else:
            return redirect('/')

    """
        Изменение элемента меню
    """
    @route('/edit/<int:id_menu>', methods=['GET', 'POST'])
    def edit(self, id_menu):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Menu import Menu

            menu = Menu.query.filter_by(id=id_menu).first()
            form = MenuType()
            from static.Entity.Role import Role
            roles = [(r.id, r.name) for r in Role.query.all()]
            form.role.choices = roles
            if form.validate_on_submit():

                menu.name = form.name.data
                menu.route = form.route.data
                menu.role = Role().query.filter_by(id=form.role.data).first()
                menu.order = form.order.data
                from app import db
                db.session.commit()

                Logger('Изменение элемента меню')

                return redirect('/menu')
            else:
                form.name.data = menu.name
                form.route.data = menu.route
                form.role.data = menu.role.id
                form.order.data = menu.order

            return render_template('admin/menu/edit.html', title='Изменение элемента меню', form=form, menu=menu)
        else:
            return redirect('/')

    """
         Удаление элемента меню
    """

    @route('/delete/<int:id_menu>', methods=['GET'])
    def delete(self, id_menu):

        from static.Service.Security import is_granted
        if is_granted(self.DELETE):

            from static.Entity.Menu import Menu

            menu = Menu.query.filter_by(id=id_menu).first()
            from app import db
            db.session.delete(menu)
            db.session.commit()

            Logger('Удаление элемента меню')

            return redirect('/menu')
        else:
            return redirect('/')

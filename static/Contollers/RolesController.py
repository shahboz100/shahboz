# -*- coding: utf-8 -*-
from flask import render_template, redirect
from flask_classy import FlaskView, route
from static.Service.Logger import Logger
from static.Form.RoleType import RoleType


class RolesController(FlaskView):

    MODULE_NAME = 'ROLES'

    VIEW = MODULE_NAME + '_VIEW'
    UPDATE = MODULE_NAME + '_UPDATE'
    DELETE = MODULE_NAME + '_DELETE'

    """
        РОЛИ
    """
    @route('/', methods=['GET', 'POST'])
    def index(self):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):

            from static.Entity.Role import Role
            roles = Role.query.all()

            Logger('Просмотр ролей')

            return render_template('admin/role/index.html', title='Роли', roles = roles)
        else:
            return redirect('/')
    """
         Добавление роли
    """
    @route('/add', methods=['GET', 'POST'])
    def add(self):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Role import Role
            from app import db

            form = RoleType()
            if form.validate_on_submit():
                new_role = Role(name=form.name.data)

                db.session.add(new_role)
                db.session.commit()

                Logger('Добавление роли')

                return redirect('/role')

            return render_template('admin/role/add.html', title='Добавление роли', form=form)
        else:
            return redirect('/')

    """
        Изменение роли
    """
    @route('/edit/<int:id_role>', methods=['GET', 'POST'])
    def edit(self, id_role):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Role import Role

            role = Role.query.filter_by(id=id_role).first()
            form = RoleType(obj=role)

            if form.validate_on_submit():
                role.name = form.name.data

                from app import db
                db.session.commit()

                Logger('Изменение роли')

                return redirect('/role')

            return render_template('admin/role/edit.html', title='Изменение роли', form=form, role=role)
        else:
            return redirect('/')

    """
        Удаление
    """
    @route('/delete/<int:id_role>', methods=['GET'])
    def delete(self, id_role):

        from static.Service.Security import is_granted
        if is_granted(self.DELETE):

            from static.Entity.Role import Role

            role = Role.query.filter_by(id=id_role).first()
            from app import db
            db.session.delete(role)
            db.session.commit()

            Logger('Удаление роли')

            return redirect('/role')
        else:
            return redirect('/')
# -*- coding: utf-8 -*-
import datetime

from flask import render_template, redirect, session
from flask_classy import FlaskView, route
from static.Service.Logger import Logger


class OrderController(FlaskView):
    MODULE_NAME = 'ORDERS'

    VIEW = MODULE_NAME + '_VIEW'
    MY_VIEW = MODULE_NAME + '_MY_VIEW'
    UPDATE = MODULE_NAME + '_UPDATE'
    DELETE = MODULE_NAME + '_DELETE'
    CONFIRM = MODULE_NAME + '_CONFIRM'

    """
        Список заказов
    """

    @route('/')
    def index(self):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):
            from static.Entity.Orders import Orders
            orders = Orders.query.all()

            Logger('Просмотр списка заказов')

            return render_template('admin/order/index.html', title='Список заказов', orders=orders)

        return redirect('/')

    """
            Список заказов поставщика
    """

    @route('/my')
    def my_orders(self):

        from static.Service.Security import is_granted
        if is_granted(self.MY_VIEW):
            from static.Entity.Orders import Orders
            from static.Entity.User import User
            from static.Entity.Provider import Provider
            user = User.query.filter_by(id=session['id']).first()
            provider = Provider.query.filter_by(id_user=user.id).first()
            from sqlalchemy import asc, desc, and_
            orders = Orders.query.filter_by(id_provider=provider.id).order_by(asc(Orders.date)).all()

            Logger('Просмотр списка заказов')

            return render_template('provider/orders/order.html', title='Список моих заказов', orders=orders)

        return redirect('/')

    """
            Добавление заказа
    """

    @route('/add', methods=['GET', 'POST'])
    def add(self):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Orders import Orders
            from static.Form.OrderType import OrderType
            from static.Entity.Products import Products
            form = OrderType()

            from app import db
            provider = [(c.id, c.fio) for c in db.engine.execute(
                "SELECT p.id, u.fio, u.phone, p.organization, p.address FROM " + '"user"' + " u JOIN provider p ON u.id = p.id_user;")]
            product = [(c.id, c.name) for c in Products.query.all()]
            client = [(c.id, c.fio) for c in db.engine.execute(
                "SELECT p.id, u.fio, u.phone, p.name_org, p.address FROM " + '"user"' + " u JOIN client p ON u.id = p.id_user;")]
            form.provider.choices = provider
            form.product.choices = product
            form.client.choices = client

            if form.validate_on_submit():
                new_order = Orders(
                    id_product=form.product.data,
                    id_client=form.client.data,
                    id_provider=form.provider.data,
                    date=form.date.data,
                    create_date=datetime.datetime.now(),
                    count=form.count.data,
                    comment=form.comment.data
                )
                db.session.add(new_order)
                db.session.commit()

                from static.Service.Security import sendNotification
                sendNotification(new_order.provider.user.id)

                Logger('Добавление заказа')

                return redirect('/order')
            return render_template('admin/order/add.html', title='Список заказов', form=form)
        return redirect('/')

    """
        Изменение заказа
    """

    @route('/edit/<int:id_order>', methods=['GET', 'POST'])
    def edit(self, id_order):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Orders import Orders
            from static.Form.OrderType import OrderType
            from static.Entity.Products import Products
            order = Orders.query.filter_by(id=id_order).first()
            form = OrderType(obj=order)

            from app import db
            provider = [(c.id, c.fio) for c in db.engine.execute(
                "SELECT p.id, u.fio, u.phone, p.organization, p.address FROM " + '"user"' + " u JOIN provider p ON u.id = p.id_user;")]
            product = [(c.id, c.name) for c in Products.query.all()]
            client = [(c.id, c.fio) for c in db.engine.execute(
                "SELECT p.id, u.fio, u.phone, p.name_org, p.address FROM " + '"user"' + " u JOIN client p ON u.id = p.id_user;")]
            form.provider.choices = provider

            form.product.choices = product
            form.client.choices = client

            if form.validate_on_submit():
                order.id_product = form.product.data
                order.id_client = form.client.data
                order.id_provider = form.provider.data
                order.date = form.date.data
                order.count = form.count.data
                order.comment = form.comment.data

                db.session.commit()
                from static.Service.Security import sendNotificationEdit
                sendNotificationEdit(order.provider.user.id)
                Logger('Изменение заказа')

                return redirect('/order')
            return render_template('admin/order/edit.html', title='Изменение заказа', order=order, form=form)
        return redirect('/')

    """
         Удаление заказа
    """

    @route('/delete/<int:id_order>', methods=['GET'])
    def delete(self, id_order):

        from static.Service.Security import is_granted
        if is_granted(self.DELETE):

            from static.Entity.Orders import Orders
            order = Orders.query.filter_by(id=id_order).first()
            from app import db
            db.session.delete(order)
            db.session.commit()

            Logger('Удаление заказа')

            return redirect('/order')
        else:
            return redirect('/')

    """
        Просмотр заказа
    """

    @route('/show/<int:id_order>', methods=['GET', 'POST'])
    def show(self, id_order):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):
            from static.Entity.Orders import Orders
            order = Orders.query.filter_by(id=id_order).first()

            Logger('Просмотр заказа')

            from static.Form.CommentType import CommentType
            from static.Entity.Comments import Comments
            from static.Entity.User import User
            from app import db

            comments = Comments.query.filter_by(id_order=id_order).all()

            form = CommentType()
            if form.validate_on_submit():
                comment = Comments(id_user=User.query.filter_by(id=session['id']).first().id, id_order=order.id,
                                   text=form.comment.data, date=datetime.datetime.today())

                db.session.add(comment)
                db.session.commit()
                return redirect('/order/show/' + str(id_order))
            return render_template('admin/order/show.html', title='Просмотр заказа', order=order,
                                   form=form, comments=comments)
        return redirect('/')

    """
        Подтверждение заказа
    """
    @route('/confirm/<int:id_order>', methods=['GET'])
    def confirm(self, id_order):

        from static.Service.Security import is_granted
        if is_granted(self.CONFIRM):
            from static.Entity.Orders import Orders
            order = Orders.query.filter_by(id=id_order).first()
            order.confirm = 1
            from app import db
            db.session.commit()

            Logger('Подтверждение заказа')

            return redirect('/order/my')
        return redirect('/')

    """
            Прием товара
    """
    @route('/accepted/<int:id_order>', methods=['GET'])
    def accepted(self, id_order):

        from static.Service.Security import is_granted
        if is_granted(self.CONFIRM):
            from static.Entity.Orders import Orders
            order = Orders.query.filter_by(id=id_order).first()
            order.accepted = 1
            from app import db
            db.session.commit()

            Logger('Прием заказа')

            return redirect('/order')
        return redirect('/')

    """
            Просмотр заказа
    """

    @route('/show_my/<int:id_order>', methods=['GET', 'POST'])
    def show_my(self, id_order):

        from static.Service.Security import is_granted
        if is_granted(self.MY_VIEW):
            from static.Entity.Orders import Orders
            from static.Entity.Comments import Comments

            order = Orders.query.filter_by(id=id_order).first()
            comments = Comments.query.filter_by(id_order=id_order).all()

            Logger('Просмотр заказа')
            from static.Form.CommentType import CommentType
            from static.Entity.Comments import Comments
            from static.Entity.User import User
            from app import db

            form = CommentType()
            if form.validate_on_submit():

                comment = Comments(id_user=User.query.filter_by(id=session['id']).first().id, id_order=order.id, text=form.comment.data)

                db.session.add(comment)
                db.session.commit()
                return redirect('/order/show_my/'+str(id_order))

            return render_template('provider/orders/show.html', title='Просмотр заказа', order=order, comments=comments,
                                   form=form)
        return redirect('/')

    @route("/delete/<int:id_p>/comment/<int:id_c>")
    def deleteComment(self, id_p, id_c):
        from static.Entity.Comments import Comments
        from app import db
        comment = Comments.query.filter_by(id=id_c).first()
        db.session.delete(comment)
        db.session.commit()
        return redirect('/order/show/'+str(id_p))

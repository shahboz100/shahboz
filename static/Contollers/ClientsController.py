# -*- coding: utf-8 -*-
import datetime

from flask import render_template, redirect
from flask_classy import FlaskView, route
from werkzeug.security import generate_password_hash
from static.Service.Logger import Logger


class ClientsController(FlaskView):

    MODULE_NAME = 'CLIENTS'

    VIEW = MODULE_NAME + '_VIEW'
    UPDATE = MODULE_NAME + '_UPDATE'
    DELETE = MODULE_NAME + '_DELETE'

    """
        Список клиентов
    """
    @route('/')
    def client_index(self):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):

            from app import db
            clientss = db.engine.execute(
                "SELECT c.id, u.fio, u.phone, c.name_org, c.address, c.id c_id FROM " + '"user"' + " u JOIN client c ON u.id = c.id_user").fetchall()

            Logger('Просмотр списка клиента')

            return render_template('admin/client/index.html', title='Список клиентов', clients=clientss)
        return redirect('/')

    """
        Добавление клиента
    """
    @route('/add', methods=['GET', 'POST'])
    def add(self):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):
            from static.Form.ClientType import ClientType
            form = ClientType()
            from static.Entity.Role import Role
            role = [(r.id, r.name) for r in Role.query.all()]
            form.id_role.choices = role
            if form.validate_on_submit():
                from static.Entity.User import User
                salt1 = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
                hashed_password = generate_password_hash(form.password.data, method='sha256')
                new_user = User(login=form.login.data, password=hashed_password, salt=salt1, fio=form.fio.data,
                                email=form.email.data, phone=form.phone.data, id_role=form.id_role.data, status=1)
                from app import db
                db.session.add(new_user)
                db.session.commit()
                user = User.query.filter_by(login=new_user.login).first()
                from static.Entity.Client import Client
                new_client = Client(id_user=user.id, name_org=form.name_org.data, address=form.address.data)
                db.session.add(new_client)
                db.session.commit()

                Logger('Добавление клиента')

                return redirect('/client')
            return render_template('admin/client/add.html', title='Добавление клиента', form=form)
        return redirect('/')

    """
        Изменеие клиента
    """
    @route('/edit/<int:id_client>', methods=['GET', 'POST'])
    def edit(self, id_client):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Client import Client
            client = Client.query.filter_by(id=id_client).first()
            from static.Entity.User import User
            user = User.query.filter_by(id=client.id_user).first()
            from app import db
            from static.Form.ClientType import ClientType
            form = ClientType()
            from static.Entity.Role import Role
            roles = [(r.id, r.name) for r in Role.query.all()]
            form.id_role.choices=roles
            if form.validate_on_submit():
                user.login = form.login.data
                salt1 = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
                hashed_password = generate_password_hash(form.password.data, method='sha256')
                user.password = hashed_password
                user.salt = salt1
                user.fio = form.fio.data
                user.email = form.email.data
                user.phone = form.phone.data
                user.id_role = form.id_role.data
                client.name_org = form.name_org.data
                client.address = form.address.data
                from app import db
                db.session.commit()

                Logger('Изменение клиента')

                return redirect('/client')
            else:
                form.login.data = user.login
                form.password.data = user.password
                form.fio.data = user.fio
                form.email.data = user.email
                form.phone.data = user.phone
                form.id_role.choices = roles
                form.name_org.data = client.name_org
                form.address.data = client.address

                return render_template('admin/client/edit.html', title='Изменение клиента', client=client, form=form)
        return redirect('/')

    """
         Удаление клиента
    """
    @route('/delete/<int:id_client>', methods=['GET'])
    def delete(self, id_client):

        from static.Service.Security import is_granted
        if is_granted(self.DELETE):
            from static.Entity.Client import Client
            from static.Entity.User import User
            client = Client.query.filter_by(id=id_client).first()
            user = User.query.filter_by(id=client.id_user).first()
            from app import db
            db.session.delete(client)
            db.session.delete(user)
            db.session.commit()

            Logger('Удаление клиента')

            return redirect('/client')
        else:
            return redirect('/')

    """
        Просмотр клиента
    """
    @route('/show/<int:id_client>', methods=['GET'])
    def show(self, id_client):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):
            from app import db
            client = db.engine.execute(
                "SELECT c.id, u.fio, u.phone, u.email, (SELECT name FROM role r WHERE r.id=u.id_role) id_role, c.name_org, c.address, c.id c_id FROM " + '"user"' + " u JOIN client c ON u.id = c.id_user WHERE c.id=" + str(
                    id_client) + ";")

            Logger('Просмотр клиента')

            return render_template('admin/client/show.html', title='Просмотр клиента', client=client)
        return redirect('/')

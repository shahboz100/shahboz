# -*- coding: utf-8 -*-
from flask import request, make_response, render_template, redirect
from flask_classy import FlaskView, route
from static.Service.Logger import Logger


class CategoriesController(FlaskView):

    MODULE_NAME = 'CATEGORIES'

    VIEW = MODULE_NAME + '_VIEW'
    UPDATE = MODULE_NAME + '_UPDATE'
    DELETE = MODULE_NAME + '_DELETE'

    """
        Список категорий
    """
    @route('/')
    def index(self):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):
            from static.Entity.Category import Category
            categories = Category.query.all()

            return render_template('admin/category/index.html', title='Список категорий', categories=categories)
        return redirect('/')

    """
            Добавление категорий
    """

    @route('/add', methods=['GET', 'POST'])
    def add(self):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Form.CategoryType import CategoryType
            form = CategoryType()
            if form.validate_on_submit():
                from static.Entity.Category import Category
                category = Category(name=form.name.data)
                from app import db
                db.session.add(category)
                db.session.commit()

                Logger('Добавление категории')

                return redirect('/category')
            return render_template('admin/category/add.html', title='Добавление категории', form=form)
        return redirect('/')

    """
        Изменеие категории
    """

    @route('/edit/<int:id_category>', methods=['GET', 'POST'])
    def edit(self, id_category):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Category import Category
            category = Category.query.filter_by(id=id_category).first()

            from static.Form.CategoryType import CategoryType
            form = CategoryType(obj=category)
            if form.validate_on_submit():
                from app import db
                category.name = form.name.data
                db.session.commit()

                Logger('Изменение категории')

                return redirect('/category')

            return render_template('admin/category/edit.html', title='Изменение категории', category=category,
                                   form=form)
        return redirect('/')

    """
         Удаление категории
    """

    @route('/delete/<int:id_category>', methods=['GET'])
    def delete(self, id_category):

        from static.Service.Security import is_granted
        if is_granted(self.DELETE):

            from static.Entity.Category import Category
            category = Category.query.filter_by(id=id_category).first()
            from app import db
            db.session.delete(category)
            db.session.commit()

            Logger('Удаление категории')

            return redirect('/category')
        else:
            return redirect('/')

# -*- coding: utf-8 -*-
import datetime

from flask import render_template, redirect
from flask_classy import FlaskView, route
from werkzeug.security import generate_password_hash
from static.Service.Logger import Logger


class ProvidersController(FlaskView):

    MODULE_NAME = 'PROVIDERS'

    VIEW = MODULE_NAME + '_VIEW'
    UPDATE = MODULE_NAME + '_UPDATE'
    DELETE = MODULE_NAME + '_DELETE'

    """
        Список поставщиков
    """
    @route('/')
    def index(self):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):
            from app import db
            providers = db.engine.execute("SELECT p.id, u.fio, u.phone, p.organization, p.address FROM "+'"user"'+ " u JOIN provider p ON u.id = p.id_user;").fetchall()

            Logger('Просмотр списка поставщиков')

            return render_template('admin/provider/index.html', title='Список поставщиков', providers=providers)

        return redirect('/')

    """
        Добавление поставщика
    """
    @route('/add', methods=['GET', 'POST'])
    def add(self):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):
            from static.Form.ClientType import ClientType
            from static.Entity.Role import Role
            role = [(r.id, r.name) for r in Role.query.all()]
            form = ClientType()
            form.id_role.choices=role
            if form.validate_on_submit():
                from static.Entity.User import User
                salt1 = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
                hashed_password = generate_password_hash(form.password.data, method='sha256')
                new_user = User(login=form.login.data, password=hashed_password, salt=salt1, fio=form.fio.data,
                                email=form.email.data, phone=form.phone.data, id_role=form.id_role.data, status=1)
                from app import db
                db.session.add(new_user)
                db.session.commit()
                user = User.query.filter_by(login=new_user.login).first()
                from static.Entity.Provider import Provider
                new_client = Provider(id_user=user.id, organization=form.name_org.data, address=form.address.data)
                db.session.add(new_client)
                db.session.commit()

                Logger('Добавление поставщика')

                return redirect('/provider')
            return render_template('admin/provider/add.html', title='',form=form)
        return redirect('/')

    """
        Изменеие поставщика
    """
    @route('/edit/<int:id_provider>', methods=['GET', 'POST'])
    def edit(self, id_provider):

        from static.Service.Security import is_granted
        if is_granted(self.UPDATE):

            from static.Entity.Provider import Provider
            provider = Provider.query.filter_by(id=id_provider).first()
            from static.Entity.User import User
            user = User.query.filter_by(id=provider.id_user).first()
            from app import db
            from static.Form.ClientType import ClientType
            from static.Entity.Role import Role
            roles = [(c.id, c.name) for c in Role.query.all()]
            form = ClientType(user=user, client=provider)
            form.id_role.choices = roles
            if form.validate_on_submit():
                user.login = form.login.data
                salt1 = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
                hashed_password = generate_password_hash(form.password.data, method='sha256')
                user.password = hashed_password
                user.salt = salt1
                user.fio = form.fio.data
                user.email = form.email.data
                user.phone = form.phone.data
                user.id_role = form.id_role.data
                provider.organization = form.name_org.data
                provider.address = form.address.data
                from app import db
                db.session.commit()

                Logger('Изменение поставщика')

                return redirect('/provider')
            else:
                form.login.data = user.login
                form.password.data = user.password
                form.fio.data = user.fio
                form.email.data = user.email
                form.phone.data = user.phone
                form.id_role.choices = roles
                form.name_org.data = provider.organization
                form.address.data = provider.address

            return render_template('admin/provider/edit.html', title='Изменение поставщика', provider=provider, form=form)
        return redirect('/')

    """
        Удаление поставщика
    """
    @route('/delete/<int:id_provider>', methods=['GET'])
    def delete(self, id_provider):

        from static.Service.Security import is_granted
        if is_granted(self.DELETE):

            from static.Entity.Provider import Provider
            from static.Entity.User import User
            provider = Provider.query.filter_by(id=id_provider).first()
            user = User.query.filter_by(id=provider.id_user).first()
            from app import db
            db.session.delete(provider)
            db.session.delete(user)
            db.session.commit()

            Logger('Удаление поставщика')

            return redirect('/provider')
        else:
            return redirect('/')

    """
        Просмотр поставщиков
    """
    @route('/show/<int:id_provider>', methods=['GET'])
    def show(self, id_provider):

        from static.Service.Security import is_granted
        if is_granted(self.VIEW):
            from static.Entity.Provider import Provider
            provider = Provider.query.filter_by(id=id_provider).first()

            Logger('Просмотр списка поставщиков')

            return render_template('admin/provider/show.html', title='Просмотр поставщика', provider=provider)
        return redirect('/')

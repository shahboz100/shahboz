from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, PasswordField
from wtforms.validators import DataRequired


class RegistrationType(FlaskForm):
    is_client = SelectField(coerce=int)
    login = StringField('Логин', validators=[DataRequired()])
    password = PasswordField('Пароль', validators=[DataRequired()])
    confirm = PasswordField('Повторите пароль', validators=[DataRequired()])
    fio = StringField('ФИО', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired()])
    phone = StringField('Телефон', validators=[DataRequired()])
    organization = StringField('Организация', validators=[DataRequired()])
    address = StringField('Адрес', validators=[DataRequired()])
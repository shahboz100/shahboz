from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SelectField
from wtforms.validators import DataRequired


class MenuType(FlaskForm):
    name = StringField('Название', validators=[DataRequired()])
    route = StringField('Адрес', validators=[DataRequired()])
    role = SelectField('Роль', coerce=int)
    order = IntegerField('Сортировка')
from flask_wtf import FlaskForm
from wtforms import SelectField


class Role_PermissionType(FlaskForm):
    role = SelectField('Роль', coerce=int)
    permission = SelectField('Разрешение', coerce=int)
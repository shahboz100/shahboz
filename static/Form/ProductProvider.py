from flask_wtf import FlaskForm
from wtforms import SelectField
from wtforms.validators import DataRequired


class ProductProvider(FlaskForm):
    provider = SelectField('Выберите поставщика', coerce=int)
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired


class CategoryType(FlaskForm):
    name = StringField('Название категории', validators=[DataRequired()])


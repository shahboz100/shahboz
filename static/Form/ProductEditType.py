from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired


class ProductType(FlaskForm):
    name = StringField('Название товара', validators=[DataRequired()])
    code = StringField('Артикул', validators=[DataRequired()])
    price = StringField('Цена', validators=[DataRequired()])
    count = StringField('Количество', validators=[DataRequired()])
    category = SelectField('Категория', coerce=int)
    unit = StringField('Единица изменения', validators=[DataRequired()])
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SelectField


class ClientType(FlaskForm):
    login = StringField('Логин')
    password = PasswordField('Пароль')
    fio = StringField('ФИО')
    email = StringField('Электронная почта')
    phone = StringField('Телефон')
    id_role = SelectField('Выберите роль', coerce=int)
    name_org = StringField('Название организации')
    address = StringField('Адрес')


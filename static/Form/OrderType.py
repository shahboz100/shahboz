from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, DateField, TextAreaField, validators
from wtforms.validators import DataRequired


class OrderType(FlaskForm):
    product = SelectField('Товар', coerce=int)
    provider = SelectField('Поставщик', coerce=int)
    client = SelectField('Клиент', coerce=int)
    date = DateField('Дата поставки', format='%m/%d/%Y', validators=(validators.Optional(),))
    count = StringField('Количество', validators=[DataRequired()])
    comment = TextAreaField('Комментарий', validators=[DataRequired()])